package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

public class Time {
    private final HashMap<String, Jogador> jogadores = new HashMap<>();

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    public void addJogador(String x, Jogador y) {
        this.jogadores.put(x, y);
    }
    
}
